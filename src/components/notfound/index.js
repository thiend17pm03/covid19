import React from "react";
import "./index.css";
import Footer from "../footer";
import Header from "../header";
const NotF = () => {
  return (
    <>
      <Header />
      <div className="bodyhi  row" style={{ margin: "0px" }}>
        <div className="col-12">
          <div className="number">404</div>
          <div className="text">Page not found</div>
          <div className="text">This may not mean anything.</div>
          <div className="text">
            I'm probably working on something that has blown up.
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default NotF;
