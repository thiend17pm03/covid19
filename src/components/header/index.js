import React from "react";
import "../header/index.css";
import img1 from "./virut.png";
import img2 from "./logoBYT2.png";
import { Link } from "react-router-dom";
const Header = () => {
  //<button className="col-2 bntnavbar">Trang chủ</button>
  //<button className="col-2 bntnavbar">Thông tin chi tiết</button>
  return (
    <>
      <div className="bgrHeader row" style={{ margin: "0px" }}>
        <div className="col-1"></div>
        <div className="col-2 ">
          <img src={img1} alt="img Virut"></img>
        </div>
        <div className="col-6">
          TRANG THÔNG TIN VỀ DỊCH BỆNH <br />
          VIÊM ĐƯỜNG HÔ HẤP CẤP COVID-19
        </div>
        <div className="col-2  ">
          <img
            src={img2}
            alt="img Logo BYT"
            style={({ height: "120px" }, { width: "120px" })}
          ></img>
        </div>
        <div className="col-1"></div>
      </div>
      <div className="navbar row" style={{ margin: "0px" }}>
        <div className="col-1"></div>
        <Link to="/">
          <a className="col-2 ">Trang chủ</a>
        </Link>

        <Link to="/thongtinchitiet">
          <a className="col-2 ">Thông tin chi tiết</a>
        </Link>
        <Link to="/bandodichbenh">
          <a className="col-2 ">Bản đồ dịch bệnh</a>
        </Link>
        <Link to="/tintuc">
          <a className="col-2 ">Tin tức</a>
        </Link>
        <Link to="/hotro">
          <a className="col-2 ">Hỗ trợ</a>
        </Link>

        <div className="col-1"></div>
      </div>
    </>
  );
};
export default Header;
