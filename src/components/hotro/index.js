import React from "react";
import Footer from "../footer";
import Header from "../header";
import "./index.css";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import "react-notifications/lib/notifications.css";
const HoTro = () => {
  return (
    <>
      {" "}
      <NotificationContainer />
      <Header />
      <div className="row bgr-hotro">
        <div className="col-3"></div>
        <div className="col-6">
          <textarea
            className="form-control"
            rows="10"
            cols="4"
            wrap="hard"
            id="txtGopY"
            placeholder="Nhập thông tin cần hỗ trợ"
            style={{ resize: "none" }}
          ></textarea>
          <div style={{ marginTop: "20px" }}>
            <button
              type="button"
              className="btn btn-success btn-lg"
              onClick={() => {
                document.getElementById("txtGopY").value = "";
                NotificationManager.success(
                  "Cảm ơn bạn đã góp ý",
                  "Thông báo",
                  3000
                );
              }}
            >
              Gửi hỗ trợ
            </button>
          </div>
        </div>

        <div className="col-3"></div>
      </div>
      <Footer />
    </>
  );
};
export default HoTro;
