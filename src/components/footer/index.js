import React from "react";
import { Link } from "react-router-dom";
const Footer = () => {
  return (
    <footer>
      <div className="footer-top">
        <div className="container">
          <div className="row">
            <div className="col-md-3 footer-about wow fadeInUp animated">
              <p>
                Chúng tôi luôn cập nhật tin tức dịch bệnh NCOVID-19 liên tục,
                nhanh chóng với độ chính xác cao. Cảm ơn các bạn đã quan tâm
                theo dõi.
              </p>
              <p>
                <a href="https://fb.com/daylalinkfacecuathien">Về chúng tôi</a>
              </p>
            </div>
            <div className="col-md-4 offset-md-1 footer-contact wow fadeInDown animated">
              <h3 className="">Liên hệ</h3>
              <p>
                <i className="fas fa-map-marker-alt"></i> Tân Hiệp, Tân Uyên,
                Bình Dương
              </p>
              <p>
                <i className="fas fa-phone"></i> Phone: 0978973382
              </p>
              <p>
                <i className="fas fa-envelope"></i> Email:
                <a href="mailto:thiend17pm03@gmail.com">
                  thiend17pm03@gmail.com
                </a>
              </p>
              <p>
                <i className="fab fa-skype"></i> Skype: thiend17pm03
              </p>
            </div>
            <div className="col-md-4 footer-links wow fadeInUp animated">
              <div className="row">
                <div className="col">
                  <h3>Links</h3>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <p>
                    <Link to="/">Trang chủ</Link>
                  </p>
                  <p>
                    <Link to="/bandodichbenh">Bản đồ dịch bệnh</Link>
                  </p>
                  <p>
                    <Link to="/thongtinchitiet">
                      Thông tin chi tiết Việt Nam
                    </Link>
                  </p>
                  <p>
                    <Link to="/hotro">Hỗ trợ, góp ý</Link>
                  </p>
                </div>
                <div className="col-md-6"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container">
          <div className="row">
            <div className="col-md-6 footer-copyright">
              Copyright © 2020 by Hoàng Thiện
            </div>
            <div className="col-md-6 footer-social">
              <a href="https://fb.com/daylalinkfacecuathien">
                <i className="fab fa-facebook-f"></i>
              </a>

              <Link to="/">
                <i className="fab fa-twitter"></i>
              </Link>
              <Link to="/">
                <i className="fab fa-google-plus-g"></i>
              </Link>
              <Link to="/">
                <i className="fab fa-instagram"></i>
              </Link>
              <Link to="/">
                <i className="fab fa-pinterest"></i>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
