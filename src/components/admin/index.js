import React, { useState, useEffect } from "react";
import Footer from "../footer";
import AdBenhNhan from "./adbenhnhan";
import AdDiaPhuong from "./addiaphuong";
import "./index.css";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
const Admin = () => {
  const load = useSelector((state) => state.account.id);
  const [keyShow, setKeyShow] = useState(0);
  const showInfo = () => {
    if (keyShow === 0) return <AdBenhNhan />;
    return <AdDiaPhuong />;
  };

  useEffect(() => {
    showInfo();
  }, [keyShow]);
  let history = useHistory();
  const check = () => {
    if (load === "") history.push("/dangnhapadmin");
  };
  useEffect(() => {
    check();
  }, []);
  if (load === "") {
    return <></>;
  }
  return (
    <>
      <div className="container-ad " style={{ paddingTop: "0px" }}>
        <div className="row tab-link">
          <div
            className={
              keyShow === 0
                ? "tab-link--key0 tab-link--active0"
                : "tab-link--key0 "
            }
            onClick={() => {
              setKeyShow(0);
            }}
          >
            Cập nhật thông tin bệnh nhân
          </div>
          <div
            className={
              keyShow === 0
                ? "tab-link--key1 "
                : "tab-link--key1 tab-link--active1"
            }
            onClick={() => {
              setKeyShow(1);
            }}
          >
            Cập nhật thông tin địa phương
          </div>
          <div className="tab-link--key1 ">Tin tức</div>
          <div className="tab-link--key1 ">Xem hỗ trợ</div>
        </div>
        {showInfo()}
      </div>
      <script></script>
      <Footer />
    </>
  );
};
export default Admin;
