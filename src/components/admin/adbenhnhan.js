import React, { useState, useEffect } from "react";
import $$ from "jquery";
import firebase from "../../util/firebase";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import "react-notifications/lib/notifications.css";

const AdBenhNhan = () => {
  const [dataBN, setDataBN] = useState(null);
  const [benhNhan, setBenhNhan] = useState(null);
  const [keyBN, setKeyBN] = useState(null);

  const getDataBN = () => {
    const dbBN = firebase.database().ref("BenhNhan");
    dbBN.on("value", (snapshot) => {
      setDataBN(snapshot.val());
    });
  };
  const checkInput = () => {
    let mess = "";
    if (document.getElementById("txtMaADBN").value.trim() === "")
      mess += "Nhập mã bệnh nhân \n";
    if (document.getElementById("txtTenADBN").value.trim() === "")
      mess += "Nhập tên bệnh nhân  \n";
    if (document.getElementById("txtTuoiADBN").value.trim() === "")
      mess += "Nhập tuổi  \n";
    if (document.getElementById("txtDiaPhuongADBN").value.trim() === "")
      mess += "Nhập địa phương  \n";
    if (document.getElementById("txtQuocTichADBN").value.trim() === "")
      mess += "Nhập  quốc tịch \n";

    if (mess !== "") {
      NotificationManager.warning(
        "Không được để trống các trường bắt buộc",
        "Nhấn để xem chi tiết!",
        5000,
        () => {
          alert(mess);
        }
      );
      return false;
    }
    return true;
  };

  const resetDetails = () => {
    document.getElementById("txtDiaPhuongADBN").value = "";
    document.getElementById("txtLichTrinhADBN").value = "";
    document.getElementById("txtMaADBN").value = "";
    document.getElementById("txtQuocTichADBN").value = "";
    document.getElementById("txtTenADBN").value = "";
    document.getElementById("txtThongTinChiTietADBN").value = "";
    document.getElementById("txtTuoiADBN").value = "";
    document.getElementById("sltStatusADBN").value = "0";
    document.getElementById("sltGioiTinh").value = "0";
    document.getElementById("btnThemMoiADBN").disabled = false;
    document.getElementById("btnXoaADBN").disabled = true;
    document.getElementById("btnCapNhatADBN").disabled = true;
    setBenhNhan(null);
    setKeyBN(null);
  };
  const convertObj = (move, key) => {
    if (key === 0) {
      let temparr = [];

      try {
        move.map((item) => {
          temparr.push(
            "" + item.name + "*" + item.Date + "*" + item.lat + "*" + item.long
          );
        });
      } catch (e) {}
      return temparr.join("|");
    } else {
      let temparr = [];
      try {
        let temparr1 = move.split("|");
        temparr1.map((item) => {
          let temp2 = item.split("*");
          if (temp2.length === 4) {
            temparr.push({
              name: temp2[0],
              Date: temp2[1],
              lat: temp2[2],
              long: temp2[3],
            });
          }
        });
      } catch (e) {}
      return temparr;
    }
  };
  const getNewBN = () => {
    return {
      city: document.getElementById("txtDiaPhuongADBN").value.trim(),
      move: convertObj(
        document.getElementById("txtLichTrinhADBN").value.trim(),
        1
      ),
      country: document.getElementById("txtQuocTichADBN").value.trim(),
      name: document.getElementById("txtTenADBN").value.trim(),
      info: document.getElementById("txtThongTinChiTietADBN").value.trim(),
      age: document.getElementById("txtTuoiADBN").value + "",
      status: document.getElementById("sltStatusADBN").value.trim(),
      sex: document.getElementById("sltGioiTinh").value.trim(),
    };
  };
  const showDetails = () => {
    if (!benhNhan) {
      document.getElementById("btnThemMoiADBN").disabled = false;
      document.getElementById("btnXoaADBN").disabled = true;
      document.getElementById("btnCapNhatADBN").disabled = true;
      document.getElementById("txtMaADBN").disabled = false;
    } else {
      document.getElementById("txtDiaPhuongADBN").value = benhNhan.city;
      document.getElementById("txtLichTrinhADBN").value = convertObj(
        benhNhan.move,
        0
      );
      document.getElementById("txtMaADBN").value = benhNhan.id;
      document.getElementById("txtQuocTichADBN").value = benhNhan.country;
      document.getElementById("txtTenADBN").value = benhNhan.name;
      document.getElementById("txtThongTinChiTietADBN").value = benhNhan.info;
      document.getElementById("txtTuoiADBN").value = benhNhan.age;
      document.getElementById("sltStatusADBN").value = "" + benhNhan.status;
      document.getElementById("sltGioiTinh").value = "" + benhNhan.sex;
      document.getElementById("btnThemMoiADBN").disabled = true;
      document.getElementById("btnXoaADBN").disabled = false;
      document.getElementById("btnCapNhatADBN").disabled = false;
      document.getElementById("txtMaADBN").disabled = true;
    }
  };
  const showListBenhNhan = () => {
    if (dataBN === null)
      return (
        <tr>
          {""}
          <td colSpan="5">
            {`Đang tải dữ liệu... `}
            {""}
            <div className="spinner-grow text-success"></div>
            <div className="spinner-grow text-info"></div>
            <div className="spinner-grow text-warning"></div>
            <div className="spinner-grow text-danger"></div>
          </td>
        </tr>
      );
    const tempList = [];
    for (const item in dataBN) {
      if (dataBN.hasOwnProperty(item)) {
        let stt =
          dataBN[item].status == 0
            ? "Đang điều trị"
            : item.status == 1
            ? "Khỏi"
            : "Tử vong";
        tempList.push(
          <tr
            key={item}
            onClick={() => {
              setBenhNhan(Object.assign({ id: item }, dataBN[item]));
              setKeyBN(item);
            }}
          >
            <td>{item}</td>
            <td>{dataBN[item].age}</td>
            <td>{dataBN[item].city}</td>
            <td>{stt}</td>
            <td>{dataBN[item].country}</td>
          </tr>
        );
      }
    }
    return tempList;
  };

  useEffect(() => {
    getDataBN();
  }, []);
  useEffect(() => {
    showDetails();
  }, [benhNhan]);
  $$(document).ready(function () {
    $$("#ipAD1").on("keyup", function () {
      var value = $$(this).val().toLowerCase();
      $$("#tbAD1 tr").filter(function () {
        $$(this).toggle($$(this).text().toLowerCase().indexOf(value) > -1);
      });
    });
  });
  return (
    <>
      {" "}
      <NotificationContainer />
      <div className=" row  from-ad-bn w3-animate-right">
        <div className="col-5 col-ad-bn1">
          <div className="row row-form  ">
            <div className="col-3 col-if d-flex align-items-center justify-content-end">
              {" "}
              Mã bệnh nhân :{" "}
            </div>
            <div className="col-3">
              <input
                type="text"
                className="form-control"
                placeholder="Nhập mã bệnh nhân"
                id="txtMaADBN"
              />
            </div>
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Trạng Thái :{" "}
            </div>
            <div className="col-4">
              <select
                className="form-control"
                id="sltStatusADBN"
                defaultValue="0"
              >
                <option value="0">Đang điều trị</option>
                <option value="1">Khỏi</option>
                <option value="2">Tử vong</option>
              </select>
            </div>
          </div>
          <div className="row row-form  ">
            <div className="col-3 col-if d-flex align-items-center justify-content-end">
              {" "}
              Tên bệnh nhân :{" "}
            </div>
            <div className="col-6">
              <input
                type="text"
                className="form-control"
                placeholder="Nhập tên bệnh nhân"
                id="txtTenADBN"
              />
            </div>
          </div>
          <div className="row row-form  ">
            <div className="col-3 col-if d-flex align-items-center justify-content-end">
              {" "}
              Tuổi :{" "}
            </div>
            <div className="col-3">
              <input
                type="number"
                min="0"
                className="form-control"
                placeholder="Nhập tuổi"
                id="txtTuoiADBN"
              />
            </div>{" "}
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Giới tính :{" "}
            </div>
            <div className="col-3">
              <select
                className="form-control"
                id="sltGioiTinh"
                defaultValue="0"
              >
                <option value="0">Nam</option>
                <option value="1">Nữ</option>
              </select>
            </div>
          </div>

          <div className="row row-form  ">
            <div className="col-3 col-if d-flex align-items-center justify-content-end">
              {" "}
              Địa phương :{" "}
            </div>
            <div className="col-3">
              <input
                type="text"
                className="form-control"
                placeholder="Nhập Tỉnh/TP"
                id="txtDiaPhuongADBN"
              />
            </div>{" "}
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Quốc tịch :{" "}
            </div>
            <div className="col-4">
              <input
                type="text"
                className="form-control"
                placeholder="Nhập quốc tịch"
                id="txtQuocTichADBN"
              />
            </div>
          </div>
        </div>
        <div className="col-7">
          <div className="row row-form  ">
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Lịch trình di chuyển :{" "}
            </div>
            <div className="col-10">
              <input
                type="text"
                className="form-control"
                id="txtLichTrinhADBN"
                placeholder="Nhập lịch trình cách nhau bởi dấu | và mỗi cái cách nhau bởi dấu * ex: name*Date*lat*long "
              />
            </div>
          </div>{" "}
          <div className="row row-form  ">
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Thông tin chi tiết bệnh nhân :{" "}
            </div>
            <div className="col-10">
              <textarea
                className="form-control"
                rows="6"
                id="txtThongTinChiTietADBN"
                wrap="hard"
                placeholder="Nhập thông tin chi tiết của bệnh nhân"
                style={{ resize: "none" }}
              ></textarea>
            </div>
          </div>
        </div>
      </div>
      <div className="row fixrow-bn">
        <div className="col-9 w3-animate-zoom cad1">
          <input
            className="form-control"
            id="ipAD1"
            type="text"
            placeholder="Tìm kiếm bệnh nhân ..."
          />
          <div className=" table-ad table-hover ">
            <table className="table  ">
              <thead className="thead-light">
                <tr>
                  <th scope="col">Bệnh nhân</th>
                  <th scope="col">Tuổi</th>
                  <th scope="col">Địa điểm</th>
                  <th scope="col">Tình trạng</th>
                  <th scope="col">Quốc tịch</th>
                </tr>
              </thead>
              <tbody id="tbAD1">{showListBenhNhan()}</tbody>
            </table>
          </div>
        </div>
        <div className="col-3 w3-animate-bottom ">
          <div className="row row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-width btn-success btn-lg"
              id="btnThemMoiADBN"
              onClick={() => {
                if (checkInput())
                  try {
                    let maNewMaBn = document
                      .getElementById("txtMaADBN")
                      .value.trim()
                      .toUpperCase();
                    let ck = true;
                    const x = firebase.database().ref("BenhNhan");
                    x.once("value", (snapshot) => {
                      ck = snapshot.hasChild(maNewMaBn);
                    });
                    if (ck) {
                      NotificationManager.warning(
                        "Mã bệnh nhân " + maNewMaBn + " đã tồn tại",
                        "Thông báo",
                        3000
                      );
                    } else {
                      firebase
                        .database()
                        .ref("BenhNhan")
                        .child(maNewMaBn)
                        .set(getNewBN());
                      resetDetails();

                      NotificationManager.success(
                        "Thêm bệnh nhân thành công",
                        "Thông báo",
                        3000
                      );
                    }
                  } catch (e) {
                    console.log(e);
                    NotificationManager.error(
                      "",
                      "Không thể thêm bệnh nhân",
                      5000
                    );
                  }
              }}
            >
              Thêm mới
            </button>
          </div>
          <div className="row  row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-width btn-warning btn-lg"
              id="btnCapNhatADBN"
              onClick={() => {
                if (checkInput() && keyBN != null) {
                  try {
                    firebase
                      .database()
                      .ref("BenhNhan")
                      .child(keyBN)
                      .set(getNewBN());
                    NotificationManager.success(
                      "Cập nhật thông tin bệnh nhân thành công",
                      "Thông báo",
                      3000
                    );
                  } catch (e) {
                    NotificationManager.error(
                      "",
                      "Không thể cập nhật bệnh nhân",
                      5000
                    );
                  }
                }
              }}
            >
              Cập nhật
            </button>
          </div>
          <div className="row row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-width btn-danger btn-lg"
              id="btnXoaADBN"
              onClick={() => {
                if (keyBN != null) {
                  try {
                    firebase.database().ref("BenhNhan").child(keyBN).remove();
                    NotificationManager.success(
                      "Xóa bệnh nhân thành công",
                      "Thông báo",
                      3000
                    );
                    resetDetails();
                  } catch (e) {
                    NotificationManager.error(
                      "",
                      "Không thể xóa bệnh nhân",
                      5000
                    );
                  }
                }
              }}
            >
              Xóa
            </button>
          </div>
          <div className="row row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-primary btn-width btn-lg"
              id="btnResetADBN"
              onClick={() => {
                resetDetails();
                NotificationManager.info("", "Đã reset", 2000);
              }}
            >
              Reset
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default AdBenhNhan;
