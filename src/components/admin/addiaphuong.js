import React, { useState, useEffect } from "react";
import $$ from "jquery";
import firebase from "../../util/firebase";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import "react-notifications/lib/notifications.css";
const AdDiaPhuong = () => {
  const [dataDP, setDataDP] = useState(null);
  const [diaPhuong, setDiaPhuong] = useState(null);
  const [keyDP, setKeyDP] = useState(null);

  const getDataDP = () => {
    const dbBN = firebase.database().ref("DiaPhuong");
    dbBN.on("value", (snapshot) => {
      setDataDP(snapshot.val());
    });
  };
  const checkInput = () => {
    let mess = "";
    if (document.getElementById("txtTenTinhADDP").value.trim() === "")
      mess += "Nhập tên địa phương \n";
    if (document.getElementById("txtMaTinhADDP").value.trim() === "")
      mess += "Nhập Mã địa phương \n";
    if (document.getElementById("txtCaNhiemADDP").value.trim() === "")
      mess += "Nhập số ca nhiễm  \n";
    if (document.getElementById("txtCaDieuTriADDP").value.trim() === "")
      mess += "Nhập  số ca đang điều trị  \n";
    if (document.getElementById("txtCakhoiADDP").value.trim() === "")
      mess += "Nhập  số ca khỏi   \n";
    if (document.getElementById("txtCaTuVongADDP").value.trim() === "")
      mess += "Nhập   số ca tử vong \n";

    if (mess !== "") {
      NotificationManager.warning(
        "Không được để trống các trường bắt buộc",
        "Nhấn để xem chi tiết!",
        5000,
        () => {
          alert(mess);
        }
      );
      return false;
    }
    return true;
  };
  const resetDetails = () => {
    document.getElementById("txtTenTinhADDP").value = "";
    document.getElementById("txtCaNhiemADDP").value = "";
    document.getElementById("txtCaDieuTriADDP").value = "";
    document.getElementById("txtCakhoiADDP").value = "";
    document.getElementById("txtCaTuVongADDP").value = "";
    document.getElementById("txtKinhDoADDP").value = "";
    document.getElementById("txtViDoADDP").value = "";
    document.getElementById("txtMaTinhADDP").value = "";
    setDiaPhuong(null);
    setKeyDP(null);
  };
  const getNewDP = () => {
    return {
      name: document.getElementById("txtTenTinhADDP").value + "",
      confirmed: document.getElementById("txtCaNhiemADDP").value + "",
      death: document.getElementById("txtCaTuVongADDP").value + "",
      active: document.getElementById("txtCaDieuTriADDP").value + "",
      recovered: document.getElementById("txtCakhoiADDP").value + "",
      long: document.getElementById("txtKinhDoADDP").value + "",
      lat: document.getElementById("txtViDoADDP").value + "",
    };
  };

  const showDetails = () => {
    if (!diaPhuong) {
      document.getElementById("btnThemMoiADDP").disabled = false;
      document.getElementById("btnXoaADDP").disabled = true;
      document.getElementById("btnCapNhatADDP").disabled = true;
      document.getElementById("txtMaTinhADDP").disabled = false;
    } else {
      document.getElementById("txtMaTinhADDP").value = "" + diaPhuong.id;
      document.getElementById("txtTenTinhADDP").value = "" + diaPhuong.name;
      document.getElementById("txtCaNhiemADDP").value =
        "" + diaPhuong.confirmed;
      document.getElementById("txtCaDieuTriADDP").value = "" + diaPhuong.active;
      document.getElementById("txtCakhoiADDP").value = "" + diaPhuong.recovered;
      document.getElementById("txtCaTuVongADDP").value = "" + diaPhuong.death;
      document.getElementById("txtKinhDoADDP").value = "" + diaPhuong.long;
      document.getElementById("txtViDoADDP").value = "" + diaPhuong.lat;
      document.getElementById("btnThemMoiADDP").disabled = true;
      document.getElementById("btnXoaADDP").disabled = false;
      document.getElementById("btnCapNhatADDP").disabled = false;
      document.getElementById("txtMaTinhADDP").disabled = true;
    }
  };
  const showListDiaPhuong = () => {
    if (dataDP === null)
      return (
        <tr>
          {""}
          <td colSpan="5">
            {`Đang tải dữ liệu... `}
            {""}
            <div className="spinner-grow text-success"></div>
            <div className="spinner-grow text-info"></div>
            <div className="spinner-grow text-warning"></div>
            <div className="spinner-grow text-danger"></div>
          </td>
        </tr>
      );
    const tempList = [];

    for (const item in dataDP) {
      if (dataDP.hasOwnProperty(item)) {
        tempList.push(
          <tr
            key={item}
            onClick={() => {
              setDiaPhuong(Object.assign({ id: item }, dataDP[item]));
              setKeyDP(item);
            }}
          >
            <td>{dataDP[item].name}</td>
            <td>{dataDP[item].confirmed}</td>
            <td>{dataDP[item].active}</td>
            <td>{dataDP[item].recovered}</td>
            <td>{dataDP[item].death}</td>
          </tr>
        );
      }
    }
    return tempList;
  };
  useEffect(() => {
    getDataDP();
  }, []);
  useEffect(() => {
    showDetails();
  }, [diaPhuong]);
  $$(document).ready(function () {
    $$("#ipAD2").on("keyup", function () {
      var value = $$(this).val().toLowerCase();
      $$("#tbAD2 tr").filter(function () {
        $$(this).toggle($$(this).text().toLowerCase().indexOf(value) > -1);
      });
    });
  });
  return (
    <>
      <NotificationContainer />
      <div className=" row  from-ad-dp w3-animate-left">
        <div className="col-6 col-ad-bn1">
          <div className="row row-form  ">
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Tên Tỉnh/TP :{" "}
            </div>
            <div className="col-4">
              <input
                type="text"
                className="form-control"
                placeholder="Nhập tên Tỉnh/TP"
                id="txtTenTinhADDP"
              />
            </div>
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Mã tỉnh :{" "}
            </div>
            <div className="col-4">
              <input
                type="text"
                className="form-control"
                placeholder="Nhập mã địa phương "
                id="txtMaTinhADDP"
              />
            </div>
          </div>
          <div className="row row-form  ">
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Số ca nhiễm :{" "}
            </div>
            <div className="col-4">
              <input
                type="number"
                min="0"
                className="form-control"
                placeholder="Nhập số ca nhiễm"
                id="txtCaNhiemADDP"
              />
            </div>{" "}
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Đang điều trị :{" "}
            </div>
            <div className="col-4">
              <input
                type="number"
                min="0"
                className="form-control"
                placeholder="Nhập số ca đang điều trị"
                id="txtCaDieuTriADDP"
              />
            </div>
          </div>{" "}
          <div className="row row-form  ">
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Số ca khỏi :{" "}
            </div>
            <div className="col-4">
              <input
                type="number"
                min="0"
                className="form-control"
                placeholder="Nhập số ca khỏi"
                id="txtCakhoiADDP"
              />
            </div>{" "}
            <div className="col-2 col-if d-flex align-items-center justify-content-end">
              {" "}
              Số ca tử vong :{" "}
            </div>
            <div className="col-4">
              <input
                type="number"
                min="0"
                className="form-control"
                placeholder="Nhập số ca tử vong"
                id="txtCaTuVongADDP"
              />
            </div>
          </div>
        </div>
        <div className="col-6">
          <div className="row row-form  ">
            <div className="col-3 col-if d-flex align-items-center justify-content-end">
              {" "}
              Longitude :{" "}
            </div>
            <div className="col-5">
              <input
                type="number"
                min="0"
                className="form-control"
                id="txtKinhDoADDP"
                placeholder=" Nhập vị trí Kinh độ của địa phương "
              />
            </div>
          </div>{" "}
          <div className="row row-form  ">
            <div className="col-3 col-if d-flex align-items-center justify-content-end">
              {" "}
              Latitude :{" "}
            </div>
            <div className="col-5">
              <input
                type="number"
                min="0"
                className="form-control"
                id="txtViDoADDP"
                placeholder=" Nhập vị trí Vĩ độ của địa phương "
              />
            </div>
          </div>
        </div>
      </div>
      <div className="row fixrow-bn">
        <div className="col-9 w3-animate-zoom cad1">
          <input
            className="form-control"
            id="ipAD2"
            type="text"
            placeholder="Tìm kiếm Địa Phương ..."
          />
          <div className=" table-ad table-hover ">
            <table className="table  ">
              <thead className="thead-light">
                <tr>
                  <th scope="col">Tỉnh, Thành phố</th>
                  <th scope="col">Số ca nhiễm</th>
                  <th scope="col">Đang điều trị</th>
                  <th scope="col">Khỏi</th>
                  <th scope="col">Tử vong</th>
                </tr>
              </thead>
              <tbody id="tbAD2">{showListDiaPhuong()}</tbody>
            </table>
          </div>
        </div>
        <div className="col-3 w3-animate-top ">
          <div className="row row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-width btn-success btn-lg"
              id="btnThemMoiADDP"
              onClick={() => {
                if (checkInput())
                  try {
                    let maNewMaDP = document
                      .getElementById("txtMaTinhADDP")
                      .value.trim();
                    let ck = true;
                    const x = firebase.database().ref("DiaPhuong");
                    x.once("value", (snapshot) => {
                      ck = snapshot.hasChild(maNewMaDP);
                    });
                    if (ck) {
                      NotificationManager.warning(
                        "Mã địa phương " + maNewMaDP + " đã tồn tại",
                        "Thông báo",
                        3000
                      );
                    } else {
                      firebase
                        .database()
                        .ref("DiaPhuong")
                        .child(maNewMaDP)
                        .set(getNewDP());
                      resetDetails();
                      NotificationManager.success(
                        "Thêm địa phương thành công",
                        "Thông báo",
                        3000
                      );
                    }
                  } catch (e) {
                    console.log(e);
                    NotificationManager.error(
                      "",
                      "Không thể thêm địa phương",
                      5000
                    );
                  }
              }}
            >
              Thêm mới
            </button>
          </div>
          <div className="row  row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-width btn-warning btn-lg"
              id="btnCapNhatADDP"
              onClick={() => {
                if (checkInput() && keyDP !== null)
                  try {
                    firebase
                      .database()
                      .ref("DiaPhuong")
                      .child(keyDP)
                      .set(getNewDP());

                    NotificationManager.success(
                      "Cập nhật địa phương thành công",
                      "Thông báo",
                      3000
                    );
                  } catch (e) {
                    console.log(e);
                    NotificationManager.error(
                      "",
                      "Không thể cập nhật địa phương",
                      5000
                    );
                  }
              }}
            >
              Cập nhật
            </button>
          </div>
          <div className="row row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-width btn-danger btn-lg"
              id="btnXoaADDP"
              onClick={() => {
                if (keyDP !== null)
                  try {
                    firebase.database().ref("DiaPhuong").child(keyDP).remove();
                    resetDetails();

                    NotificationManager.success(
                      "Xóa địa phương thành công",
                      "Thông báo",
                      3000
                    );
                  } catch (e) {
                    console.log(e);
                    NotificationManager.error(
                      "",
                      "Không thể xóa địa phương",
                      5000
                    );
                  }
              }}
            >
              Xóa
            </button>
          </div>
          <div className="row row-btn d-flex justify-content-center align-items-center">
            <button
              type="button"
              className="btn btn-primary btn-width btn-lg"
              id="btnResetADDP"
              onClick={() => {
                resetDetails();
                NotificationManager.info("", "Đã reset", 2000);
              }}
            >
              Reset
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default AdDiaPhuong;
