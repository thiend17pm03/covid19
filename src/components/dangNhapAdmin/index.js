import React from "react";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import "react-notifications/lib/notifications.css";
import Footer from "../footer";
import "./index.css";
import { useHistory } from "react-router-dom";
import firebase from "../../util/firebase";
import { useDispatch } from "react-redux";
import { setAccount } from "../../redux/action";
const DangNhapAdmin = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const checkAcc = firebase.database().ref("account");
  const checkValue = () => {
    let usr = document.getElementById("usr").value.trim() + "";
    let pws = document.getElementById("pws").value.trim() + "";
    if (usr.trim() === "" || pws.trim() === "") return false;
    return true;
  };

  return (
    <>
      {" "}
      <NotificationContainer />
      <div className="containerDN">
        <div className="boxLogin row">
          <div className="textTitle row">Đăng nhập</div>
          <div className="d-flex justify-content-center align-items-center frm">
            <form>
              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="far fa-user"></i>
                  </span>
                </div>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Nhập tài khoản"
                  id="usr"
                  name="username"
                />
              </div>

              <div className="input-group mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fas fa-key"></i>
                  </span>
                </div>
                <input
                  type="password"
                  className="form-control"
                  placeholder="Nhập mật khẩu"
                  id="pws"
                  name="password"
                />
              </div>
              <button
                type="button"
                className="btn btn-success"
                onClick={() => {
                  try {
                    if (checkValue()) {
                      let usr =
                        document.getElementById("usr").value.trim() + "";
                      let pws =
                        document.getElementById("pws").value.trim() + "";
                      try {
                        checkAcc.on("value", (snapshot) => {
                          let ck = false;
                          if (snapshot.hasChild(usr)) {
                            if (snapshot.child(usr).val() === pws) {
                              ck = true;
                              NotificationManager.success(
                                "",
                                "Đăng nhập thành công",
                                5000
                              );
                            }
                          }
                          if (ck) {
                            let action = setAccount(usr);
                            dispatch(action);
                            setTimeout(() => {
                              history.push("/admin");
                            }, 500);
                          } else
                            NotificationManager.error(
                              "Sai tên tài khoản hoặc mật khẩu",
                              "Wrong !!",
                              5000
                            );
                        });
                      } catch (err) {
                        NotificationManager.warning(
                          "",
                          "Không thể kết nối đến máy chủ!!",
                          3000
                        );
                      }
                    } else {
                      NotificationManager.error(
                        "Vui lòng nhập tài khoản và mật khẩu",
                        "Lỗi",
                        5000
                      );
                    }
                  } catch (error) {
                    console.log(error);
                  }
                }}
              >
                Tiếp tục
              </button>
            </form>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default DangNhapAdmin;
