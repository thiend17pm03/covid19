import $$ from "jquery";
import React, { useState } from "react";
import { useEffect } from "react";
import BnDetails from "./bndetails";
import firebase from "../../util/firebase";
const BenhNhan = () => {
  const [benhNhan, setBenhNhan] = useState(null);
  const [dataBN, setDataBN] = useState(null);

  const getDataBN = () => {
    const dbBN = firebase.database().ref("BenhNhan");
    dbBN.on("value", (snapshot) => {
      setDataBN(snapshot.val());
    });
  };

  const showListBenhNhan = () => {
    if (dataBN === null)
      return (
        <tr>
          {""}
          <td colSpan="5">
            {`Đang tải dữ liệu... `}
            {""}
            <div className="spinner-grow text-success"></div>
            <div className="spinner-grow text-info"></div>
            <div className="spinner-grow text-warning"></div>
            <div className="spinner-grow text-danger"></div>
          </td>
        </tr>
      );
    const tempList = [];

    for (const item in dataBN) {
      if (dataBN.hasOwnProperty(item)) {
        //console.log(dataBN.item);

        let stt =
          dataBN[item].status == 0
            ? "Đang điều trị"
            : dataBN[item].status == 1
            ? "Khỏi"
            : "Tử vong";
        tempList.push(
          <tr
            key={item}
            onClick={() => {
              setBenhNhan(Object.assign({ id: item }, dataBN[item]));
            }}
          >
            <td>{item}</td>
            <td>{dataBN[item].age}</td>
            <td>{dataBN[item].city}</td>
            <td>{stt}</td>
            <td>{dataBN[item].country}</td>
          </tr>
        );
      }
    }
    return tempList;
  };
  const showBenhNhan = () => {
    if (benhNhan === null) {
      try {
        document.getElementById("c2bn").classList.remove("col-6");
        document.getElementById("c3bn").classList.remove("col-6");
        document.getElementById("c3bn").classList.remove("pd");
        document.getElementById("c2bn").classList.add("col-10");
        document.getElementById("c2bn").classList.remove("pd");
        if (
          document.getElementById("c2bn").classList.contains("w3-animate-right")
        ) {
          document.getElementById("c2bn").classList.remove("w3-animate-right");
          void document.getElementById("c2bn").offsetWidth;
          document.getElementById("c2bn").classList.add("w3-animate-left");
        }
        document.getElementById("c1bn").classList.add("col-1");
        document.getElementById("c3bn").classList.add("col-1");
      } catch (ex) {}
      return <div></div>;
    } else {
      try {
        document.getElementById("c2bn").classList.add("col-6");

        document.getElementById("c3bn").classList.add("col-6");
        document.getElementById("c3bn").classList.add("pd");
        document.getElementById("c2bn").classList.remove("col-10");
        document.getElementById("c2bn").classList.add("pd");
        document.getElementById("c1bn").classList.remove("col-1");
        document.getElementById("c3bn").classList.remove("col-1");
        document.getElementById("c2bn").classList.remove("w3-animate-zoom");
        void document.getElementById("c2bn").offsetWidth;
        document.getElementById("c2bn").classList.add("w3-animate-right");
      } catch (ex) {}
      return <BnDetails benhNhan={benhNhan} setBenhNhan={setBenhNhan} />;
    }
  };

  useEffect(() => {
    showBenhNhan();
  }, [benhNhan]);

  useEffect(() => {
    getDataBN();
  }, []);

  $$(document).ready(function () {
    $$("#myInput").on("keyup", function () {
      var value = $$(this).val().toLowerCase();
      $$("#myTable tr").filter(function () {
        $$(this).toggle($$(this).text().toLowerCase().indexOf(value) > -1);
      });
    });
  });
  return (
    <>
      <div className="row fixrow-bn " style={{ zIndex: -1 }}>
        <div id="c1bn" className="col-1"></div>
        <div id="c2bn" className="col-10 w3-animate-zoom">
          <input
            className="form-control"
            id="myInput"
            type="text"
            placeholder="Tìm kiếm bệnh nhân ..."
          />
          <div className=" table-bns table-hover ">
            <table className="table  ">
              <thead className="thead-light">
                <tr>
                  <th scope="col">Bệnh nhân</th>
                  <th scope="col">Tuổi</th>
                  <th scope="col">Địa điểm</th>
                  <th scope="col">Tình trạng</th>
                  <th scope="col">Quốc tịch</th>
                </tr>
              </thead>
              <tbody id="myTable">{showListBenhNhan()}</tbody>
            </table>
          </div>
        </div>
        <div className="col-1" id="c3bn">
          {showBenhNhan()}
        </div>
      </div>
    </>
  );
};
export default BenhNhan;
