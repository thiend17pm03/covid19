import React, { useState, useEffect } from "react";
import $$ from "jquery";
import firebase from "../../util/firebase";
const DiaPhuong = () => {
  const [dataDP, setDataDP] = useState(null);
  const getDataDP = () => {
    const dbDP = firebase.database().ref("DiaPhuong");
    dbDP.on("value", (snapshot) => {
      setDataDP(snapshot.val());
    });
  };
  const showDiaPhuong = () => {
    if (dataDP == null)
      return (
        <tr>
          <td colSpan="5">
            {`Đang tải dữ liệu... `}
            <div className="spinner-grow text-success"></div>
            <div className="spinner-grow text-info"></div>
            <div className="spinner-grow text-warning"></div>
            <div className="spinner-grow text-danger"></div>
          </td>
        </tr>
      );
    let tempDiaPhuong = [];
    for (const item in dataDP) {
      if (dataDP.hasOwnProperty(item)) {
        tempDiaPhuong.push(
          <tr key={item}>
            <td>{dataDP[item].name}</td>
            <td>{dataDP[item].confirmed}</td>
            <td>{dataDP[item].active}</td>
            <td>{dataDP[item].recovered}</td>
            <td> {dataDP[item].death}</td>
          </tr>
        );
      }
    }
    return tempDiaPhuong;
  };

  useEffect(() => {
    getDataDP();
  }, []);
  $$(document).ready(function () {
    $$("#searchDiaPhuong").on("keyup", function () {
      var value = $$(this).val().toLowerCase();
      $$("#tableDiaPhuong tr").filter(function () {
        $$(this).toggle($$(this).text().toLowerCase().indexOf(value) > -1);
      });
    });
  });

  return (
    <>
      <div className="row fixrow-bn">
        <div id="c1bn" className="col-1"></div>
        <div id="c2bn" className="col-10 w3-animate-zoom">
          <input
            className="form-control"
            id="searchDiaPhuong"
            type="text"
            placeholder="Tìm kiếm Địa phương ..."
          />
          <div className=" table-bns table-hover ">
            <table className="table  ">
              <thead className="thead-light">
                <tr>
                  <th scope="col">Tỉnh, Thành phố</th>
                  <th scope="col">Số ca nhiễm</th>
                  <th scope="col">Đang điều trị</th>
                  <th scope="col">Khỏi</th>
                  <th scope="col">Tử vong</th>
                </tr>
              </thead>
              <tbody id="tableDiaPhuong"> {showDiaPhuong()}</tbody>
            </table>
          </div>
        </div>
        <div className="col-1" id="c3bn"></div>
      </div>
    </>
  );
};
export default DiaPhuong;
