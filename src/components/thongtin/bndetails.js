import React, { useState } from "react";
import imgMen from "./men.jpg";
import imgGirl from "./girl.jpg";
import Modal from "react-modal";
import { useHistory } from "react-router-dom";
const BnDetails = (props) => {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const history = useHistory();
  if (!props) return null;
  return (
    <>
      <Modal
        isOpen={isOpenModal}
        style={{
          overlay: { zIndex: 10 },
          content: {
            top: "20%",
            left: "20%",
            right: "auto",
            bottom: "auto",
            width: "60%",
          },
        }}
        onRequestClose={() => {
          setIsOpenModal(false);
        }}
        shouldCloseOnEsc={true}
        shouldCloseOnOverlayClick={true}
      >
        <textarea
          className="form-control"
          rows="9"
          id="txtGopYADBN"
          wrap="hard"
          placeholder="Nhập chi tiết góp ý về thông tin bệnh nhân"
          style={{ resize: "none" }}
        ></textarea>
        <div className="row row-modal d-flex align-items-center justify-content-end">
          <button
            type="button"
            className=" btn btn-success btn-lg btn-modal"
            onClick={() => setIsOpenModal(false)}
          >
            Gửi
          </button>
        </div>
      </Modal>
      <div className="bn-box w3-animate-zoom">
        <div className="bn-img-box row">
          <div className="bn-img col-4 ">
            <img src={props.benhNhan.sex == 0 ? imgMen : imgGirl} alt="avt" />
          </div>
          <div className="bn-name col-8">
            {" "}
            {`Tên : ${props.benhNhan.name} `}
            <br />
            {`Giới tính :  ${props.benhNhan.sex == 0 ? "Nam" : "Nữ"}`} <br />
            {`Tuổi :  ${props.benhNhan.age}`} <br />
            {`Địa điểm : ${props.benhNhan.city}`} <br />
            <div
              className={
                props.benhNhan.status == 0
                  ? "bn-active bn-tt"
                  : props.benhNhan.status == 1
                  ? "bn-recovered bn-tt"
                  : "bn-death bn-tt"
              }
            >
              {`Trình trạng : ${
                props.benhNhan.status == 0
                  ? "Đang điều trị"
                  : props.benhNhan.status == 1
                  ? "Khỏi"
                  : "Tử vong"
              }`}{" "}
            </div>
          </div>
        </div>
        <div className="bn-thongtin row">Thông tin chi tiết của bệnh nhân</div>
        <div className="bn-thongtin-text-box row">{props.benhNhan.info}</div>
        <div className="bn-button row">
          <div className="col-1"></div>
          <div className="col-4">
            <button
              type="button"
              className="btn btn-primary btn-lg"
              onClick={() => {
                history.push("/bandodichbenh/" + props.benhNhan.id);
              }}
            >
              Xem trên bản đồ
            </button>
          </div>
          <div className="col-3">
            {" "}
            <button
              type="button"
              className="btn btn-success btn-lg"
              onClick={() => setIsOpenModal(true)}
            >
              Góp ý
            </button>
          </div>
          <div className="col-3">
            <button
              type="button"
              className="btn btn-warning btn-lg"
              onClick={() => props.setBenhNhan(null)}
            >
              Đóng chi tiết{" "}
            </button>
          </div>
          <div className="col-1"></div>
        </div>
      </div>
    </>
  );
};
export default BnDetails;
