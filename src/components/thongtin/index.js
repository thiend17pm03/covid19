import React, { useState, useEffect } from "react";
import Footer from "../footer";
import Header from "../header";
import "./index.css";
import BenhNhan from "./thongtinbenhnhan";
import DiaPhuong from "./thongtindiaphuong";

const ThongTin = () => {
  const [keyShow, setKeyShow] = useState(0);

  const showInfo = () => {
    if (keyShow === 0) return <BenhNhan />;
    return <DiaPhuong />;
  };

  useEffect(() => {
    showInfo();
  }, [keyShow]);

  return (
    <>
      <Header />
      <div className="container-ad">
        <div className="row tab-link">
          <div
            className={
              keyShow === 0
                ? "tab-link--key0 tab-link--active0"
                : "tab-link--key0 "
            }
            onClick={() => {
              setKeyShow(0);
            }}
          >
            Xem thông tin bệnh nhân
          </div>
          <div
            className={
              keyShow === 0
                ? "tab-link--key1 "
                : "tab-link--key1 tab-link--active1"
            }
            onClick={() => {
              setKeyShow(1);
            }}
          >
            Xem thông tin địa phương
          </div>
        </div>
        {showInfo()}
      </div>

      <script></script>
      <Footer />
    </>
  );
};
export default ThongTin;
