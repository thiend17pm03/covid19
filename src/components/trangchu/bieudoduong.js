import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import "./index.css";
import axios from "axios";
import { useSelector } from "react-redux";
//import GetDataChart from "../../api/getdatachart.js";
const BieuDo = () => {
  const [dataTenDay, setDataTenDay] = useState(null);
  const key = useSelector((state) => state.country.countryKey);
  const getTenDay = () => {
    let days = [];
    let day = new Date();
    let d1 = day.getDate();
    let m1 = day.getMonth();
    let y1 = day.getFullYear();

    let month = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    let a = d1 < 10 ? "0" + d1 : "" + d1;
    let b = m1 < 10 ? "0" + (m1 + 1) : "" + (m1 + 1);
    days.push([b, a].join("-"));
    for (let j = 0; j < 10; j++) {
      if (d1 - 1 <= 0) {
        d1 = month[m1 - 1];
        m1 -= 1;
      } else d1 -= 1;
      a = d1 < 10 ? "0" + d1 : "" + d1;
      b = m1 < 10 ? "0" + (m1 + 1) : "" + (m1 + 1);
      days.push([b, a].join("-"));
    }

    let dayFrom = y1 + "-" + days[10] + "T00:00:00Z";
    let dayTo = y1 + "-" + days[1] + "T00:00:00Z";

    console.log("f : ", dayFrom, " to : ", dayTo);
    axios
      .get(
        `https://api.covid19api.com/country/${key}?from=${dayFrom}&to=${dayTo}`
      )
      .then((reponse) => {
        const arrDay = [];
        const arrConfirmed = [];
        const arrDeaths = [];
        const arrRecovered = [];
        const arrActive = [];
        let nameCountry = "";

        for (let i = 0; i < reponse.data.length; i++) {
          arrConfirmed.push(reponse.data[i].Confirmed);
          arrActive.push(reponse.data[i].Active);
          arrDeaths.push(reponse.data[i].Deaths);
          arrRecovered.push(reponse.data[i].Recovered);
          let d = reponse.data[i].Date;
          nameCountry = reponse.data[i].Country;
          // console.log(reponse.data[i].Country);
          arrDay.push(d[8] + d[9] + "/" + d[5] + d[6]);
        }

        setDataTenDay({
          confirmed: arrConfirmed,
          dayGet: arrDay,
          deaths: arrDeaths,
          recovered: arrRecovered,
          active: arrActive,
          name: nameCountry,
          total: reponse.data.length,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getTenDay();
  }, [key]);

  if (dataTenDay === null) return null;

  return (
    <>
      <div className="col-2"></div>
      <div className="bieu-do col-8">
        <Line
          data={{
            labels: dataTenDay.dayGet,

            datasets: [
              {
                data: dataTenDay.confirmed,

                label: "Số ca nhiễm",
                borderColor: "#c9302c",
                fill: true,
              },
              {
                data: dataTenDay.active,

                label: "Đang điều trị",
                borderColor: "#337ab7",
                fill: false,
              },
              {
                data: dataTenDay.recovered,

                label: "Số ca khỏi",
                borderColor: "#53ef3e",
                fill: false,
              },
              {
                data: dataTenDay.deaths,

                label: "Số người chết",
                borderColor: "#666666",
                fill: false,
              },
            ],
          }}
          options={{
            title: {
              display: true,
              text: `Thống kê tình hình dịch bệnh NCOVID của ${dataTenDay.name} trong ${dataTenDay.total} ngày cập nhật gần nhất`,
            },
            legend: {
              display: true,
              position: "bottom",
            },
          }}
        />
      </div>
      <div className="col-2 thong-tin-bieu-do">
        <div></div>
      </div>
    </>
  );
};
export default BieuDo;
