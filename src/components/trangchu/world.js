import React, { useEffect, useState } from "react";
import "./index.css";
import axios from "axios";
const World = () => {
  const [data, setData] = useState(null);
  let numberConvert = function (number) {
    let num = "";
    let t = 1;
    for (let i = number.length - 1; i >= 0; i--) {
      num += t % 3 === 0 ? number[i] + " " : number[i];
      t++;
    }
    return num.split("").reverse().join("");
  };
  const showWorld = () => {
    axios
      .get(
        "https://covid19.mathdro.id/api?fbclid=IwAR1JV6ku3MZDNfM4uyZmPU85XBoO1klOdO-VSnoFvU5eHXJeLOD3OB9Jwj0"
      )
      .then((reponse) => {
        let reco = reponse.data.recovered.value;
        let confi = reponse.data.confirmed.value;
        let dea = reponse.data.deaths.value;
        let act = parseInt(confi) - parseInt(reco) - parseInt(dea);
        let ud = reponse.data.lastUpdate;
        let temp = {
          recovered: numberConvert(reco.toString()),
          confirmed: numberConvert(confi.toString()),
          deaths: numberConvert(dea.toString()),
          active: numberConvert(act.toString()),
          lastUpdate: ud,
        };

        setData(temp);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    showWorld();
  }, []);
  if (data == null) return null;
  return (
    <>
      <hr style={{ margin: "20px 10px 20px 10px " }} />
      <div className="info2 row w3-animate-zoom" style={{ margin: "0px" }}>
        <div className="col-3 col1 ">
          <div className="text__col1">Thế giới</div>
        </div>
        <div className="col-3 col3">
          SỐ CA NHIỄM <br />
          <div className="numberI4">{data.confirmed}</div>
        </div>
        <div className="col-2 col4">
          ĐANG ĐIỀU TRỊ <br /> <div className="numberI4">{data.active}</div>
        </div>
        <div className="col-2 col5">
          KHỎI <br />
          <div className="numberI4">{data.recovered}</div>
        </div>
        <div className="col-2 col6">
          TỬ VONG <br />
          <div className="numberI4">{data.deaths}</div>
        </div>
      </div>
      <div className="last-update row">
        <div className="col-12">
          LastUpdate : "<div className="text-update"> {data.lastUpdate} </div>"
        </div>
      </div>
    </>
  );
};
export default World;
