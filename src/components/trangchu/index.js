import React from "react";
import Footer from "../footer";
import Header from "../header";
import "./index.css";
import Details from "./details.js";
import World from "./world.js";
//import BieuDo from "./bieudotron.js";
import BieuDo from "./bieudoduong.js";
const TrangChu = () => {
  return (
    <>
      <Header />
      <div className="bgr row">
        <div className="col-1"></div>
        <div className="col-10">
          <div className="infotrangchu">
            <Details />
            <World />
          </div>
          <div className="bandotrangchu row ">
            <BieuDo />
          </div>
        </div>
        <div className="col-1"></div>
      </div>
      <Footer />
    </>
  );
};
export default TrangChu;
