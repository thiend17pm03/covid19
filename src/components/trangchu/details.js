import React, { useEffect, useState } from "react";
import axios from "axios";
import "./index.css";
import { useDispatch } from "react-redux";
import { setCountry } from "../../redux/action";
const Details = () => {
  const [countries, setcountries] = useState(null);
  const [flag, setFlag] = useState({ fl: "VN", name: "Việt Nam" });
  const [data, setData] = useState(null);
  const [load, setLoad] = useState(0);
  const dispatch = useDispatch();
  const getInfoCountries = (x) => {
    axios
      .get(`https://covid19.mathdro.id/api/countries/${x}/recovered`)
      .then((reponse) => {
        setData(reponse.data[0]);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  let numberConvert = function (number) {
    let num = "";
    let t = 1;
    for (let i = number.length - 1; i >= 0; i--) {
      num += t % 3 === 0 ? number[i] + " " : number[i];
      t++;
    }

    return num.split("").reverse().join("");
  };
  const showName = () => {
    return (
      <button
        className="flagBox btn btn-danger dropdown-toggle"
        data-toggle="dropdown"
        type="button"
      >
        <img
          src={`https://www.countryflags.io/${flag.fl}/flat/64.png`}
          alt="flag"
        />
        <div className="flagText">{flag.name}</div>{" "}
        <span className="caret"></span>
      </button>
    );
  };
  const getAllCountries = () => {
    axios
      .get("https://covid19.mathdro.id/api/countries")
      .then((reponse) => {
        setcountries(reponse.data.countries);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const showCountries = () => {
    if (countries == null) return null;
    const showCT = [];

    countries.map((item) => {
      showCT.push(
        <div
          key={item.iso2}
          className="dropdown-item text-dropdown"
          onClick={() => {
            setLoad(load + 1);
            let temp = {
              fl: item.iso2,
              name: item.name,
            };
            let action = setCountry(temp.fl);
            dispatch(action);
            setFlag(temp);
          }}
        >
          {item.name}
        </div>
      );
    });
    return showCT;
  };

  useEffect(() => {
    getAllCountries();

    getInfoCountries(flag.fl);
  }, [load]);
  if (data == null) return null;
  return (
    <>
      {" "}
      <div className="info1 row w3-animate-zoom" style={{ margin: "0px" }}>
        <div className="col-3 col2  dropdown">
          {showName()}
          <div className="dropdown-menu scrollable-menu">{showCountries()}</div>
        </div>
        <div className="col-3 col3 ">
          SỐ CA NHIỄM <br />
          <div className="numberI4">
            {numberConvert(data.confirmed.toString())}
          </div>
        </div>
        <div className="col-2 col4">
          ĐANG ĐIỀU TRỊ <br />{" "}
          <div className="numberI4">
            {numberConvert(data.active.toString())}
          </div>
        </div>
        <div className="col-2 col5">
          KHỎI <br />
          <div className="numberI4">
            {numberConvert(data.recovered.toString())}
          </div>
        </div>
        <div className="col-2 col6">
          TỬ VONG <br />
          <div className="numberI4">
            {numberConvert(data.deaths.toString())}
          </div>
        </div>
      </div>
    </>
  );
};
export default Details;
