const initialState = {
  countryKey: "vn",
  list: [],
};
const countryReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET-COUNTRY": {
      //const testList = [...state.list]; // clone

      return { ...state, countryKey: action.payload };
    }
    default:
      return state;
  }
};
export default countryReducer;
