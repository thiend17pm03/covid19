const initialState = {
  id: "",
};
const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET-ACCOUNT": {
      return { ...state, id: action.payload };
    }
    default:
      return state;
  }
};
export default accountReducer;
