import { combineReducers } from "redux";
import countryReducer from "./country";
import accountReducer from "./account";

const rootReducer = combineReducers({
  country: countryReducer,
  account: accountReducer,
});
export default rootReducer;
// tổng hợp các reducer
