export const setCountry = (ct) => {
  return {
    type: "SET-COUNTRY",
    payload: ct,
  };
};
export const setAccount = (usr) => {
  return {
    type: "SET-ACCOUNT",
    payload: usr,
  };
};
