import React from "react";
import "./App.css";
import TrangChu from "./components/trangchu";

import NotF from "./components/notfound";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import ThongTin from "./components/thongtin";
import BanDo from "./components/bando";
import HoTro from "./components/hotro";
import TinTuc from "./components/tintuc";
import Admin from "./components/admin";
import DangNhapAdmin from "./components/dangNhapAdmin";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={TrangChu} />
          <Route path="/trangchu" exact component={TrangChu} />
          <Route path="/dangnhapadmin" exact component={DangNhapAdmin} />
          <Route path="/admin" exact component={Admin} />
          <Route path="/bandodichbenh" exact component={BanDo} />
          <Route path="/bandodichbenh/:id" exact component={BanDo} />
          <Route path="/thongtinchitiet" exact component={ThongTin} />
          <Route path="/tintuc" exact component={TinTuc} />
          <Route path="/hotro" exact component={HoTro} />
          <Route component={NotF} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
