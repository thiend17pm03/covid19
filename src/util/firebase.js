import firebase from "firebase/app";
import "firebase/auth"; // for authentication
import "firebase/storage"; // for storage
import "firebase/database"; // for realtime database
import "firebase/firestore"; // for cloud firestore
import "firebase/messaging"; // for cloud messaging
import "firebase/functions"; // for cloud functions

var firebaseConfig = {
  apiKey: "AIzaSyCorKVg5pdFxFI2eNddvqDsd__qr9aK3K0",
  authDomain: "ncovid-19-hoangthien.firebaseapp.com",
  databaseURL: "https://ncovid-19-hoangthien.firebaseio.com",
  projectId: "ncovid-19-hoangthien",
  storageBucket: "ncovid-19-hoangthien.appspot.com",
  messagingSenderId: "290144286303",
  appId: "1:290144286303:web:8736c0ed17a1961411cdea",
  measurementId: "G-72ZXFHQW2R",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;
