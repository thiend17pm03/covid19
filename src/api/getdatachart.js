import React, { useState, useEffect } from "react";
import axios from "axios";

export default function GetDataChart(t) {
  const [data, setData] = useState(null);
  const getData = () => {
    axios
      .get("https://covid19.mathdro.id/api/daily/08-14-2020")
      .then((reponse) => {
        setData(reponse.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getData();
  }, []);

  return data;
}
